<?php include('header.php'); ?>

<!-- Document Title
	============================================= -->
	<title>Who We Are | SL Kreativez</title>


<?php include('nav.php'); ?>


		<!-- Page Title
		============================================= -->
		<section>
				<img src="images/sl2.jpg">	

		</section><!-- #page-title end -->

		<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<div class="col_one_third">

						<div class="heading-block fancy-title nobottomborder title-bottom-border">
							<h4>Who We <span>ARE</span>.</h4>
						</div>

						<p>We're a social enterprise with interest in developing the next generation of writers and creative geniuses. Our interest is to create tools, opportunities and platforms to experiment, practice and create works of art worth celebrating and emulating.</p>
						<details>
						  <summary><a class="button button-3d noleftmargin">Read More</a></summary><br>
						  <p>Our field of interest cuts across Storytelling, Articles and Research Documentary, Nollywood and Stage Production Reviews, African Literature Reviews, Exclusive Industry Interviews, Mini-drama Productions, Creative Writing Projects, Conferences/Seminars and Art Workshops, Literacy/charity Works.</p>
						</details><br><br><br>
						<div class="clearfix"></div>

						<div class="heading-block fancy-title nobottomborder title-bottom-border">
							<h4>Our <span>Name</span>.</h4>
						</div>

						<p>SL Kreativez stands for Seekers Locus Kreativez. Seekers as a name implies people in search of a meaning, a voice and an adventure. Locus is where this can be found. Kreativez is a hype version of  Creative.In essence, we say, seekers locus is the place where creativity finds relevance, a voice and a sense of adventure.

						<details>
						  <summary><a class="button button-3d noleftmargin">Read More</a></summary><br>
						  <p>We create a platform for youngsters and the youths alike to find a voice, first, through writing, creative learning, then through other outlets as we will create in the future. </p>
						</details> <br>
						<div class="clearfix"></div>
					</div>

					<div class="col_one_third">


						<div class="heading-block fancy-title nobottomborder title-bottom-border">
							<h4>Core <span>Mission</span>.</h4>
						</div>

						<p>We want to be the energy behind the rejuvenation of the dying theatre and literary arts culture among young practitioners, graduates and undergraduates of tertiary institutions.We want to be at the forefront of awareness and promotion of literary culture in secondary school students and beyond.
						<details>
							<summary><a class="button button-3d noleftmargin">Read More</a></summary><br>
							<p>Our mission is summed up in these three point focus:
							<ul>
								<li>To energize the creative spirit of Nigerian (secondary and university students) writers, thespians and artists in their diverse field;</li>
								<li>Promote graduate enterprise in screenplays, literary arts and skill enhancement;</li>
								<li>To train and mentor art creators and promote the hunger and value for arts (stage and literary) in Nigerians</li>
							</ul>
							</p>
						</details><br><br>
						<div class="clearfix"></div>
						


						<div class="heading-block fancy-title nobottomborder title-bottom-border">
							<h4>Our <span>Story</span>.</h4>
						</div>

						<p>SL Kreativez is an offspring of the restless spirit of a Nigerian girl, by name, Esther Okoloeze. She started scribbling right from secondary school and with exposure to acting through her teenage church; she went on to study Theatre Arts from the Prestigious University of Benin where she graduated among the best of her class. 

						<details>
							<summary><a class="button button-3d noleftmargin">Read More</a></summary><br>
							<p>
							Unemployed and pushed by the mediocrity of the education system, she opted to seek something worthy to do that will not just be a means of income but bring satisfaction. Restless from all she couldn’t achieve, she is left with no other option than to take to writing and encouraged by friends on her soft sail with words; she decided to write for the fun. But somewhere down the line, she comes to a sad realization on the dying culture of theatre and creative appreciation.<br>There and then, she started asking questions and making observatory research on what can be done. This process was the most agonizing because her vision became quite too much for her to comprehend and handle. Finances were a major challenge and still are as she went on to seek for anyone interested in becoming a founder on this creative path. The response was the least favorable. Surrounded by friends who in themselves are startup founders on one project or the other and genuinely couldn’t bid on her project, she was left with no choice than to bear the cross of a selfless vision, she surges on. <br>Seekers Locus is simply a journey to finding voice and possibly fulfillment for Esther and others that her vision will benefit.<br>In her words “We’re all here to seek for something. To seek for who we are, what we’re meant to do, to become and how to become that which urges us on. We have to keep asking, seeking till we come to a locus. That nexus, a ground zero where we experience all we’ve ever seeked” And the storyline plays on…</p>
						</details><br>
						<div class="clearfix"></div>
					</div>

					<div class="col_one_third col_last">

						<div class="heading-block fancy-title nobottomborder title-bottom-border">
							<h4>Our <span>Vision</span>.</h4>
						</div>

						<p>Seekers Locus Kreativez (SLK) is a social enterprise that is based on the vision of Esther Okoloeze to create a platform where creativity in its true sense would not only thrive but the creators get exposed to resources, opportunities and get rewarded for their intellects and skills... </p>

						<details>
						  <summary><a class="button button-3d noleftmargin">Read More</a></summary><br>
						  <p> We want have a culture that appreciates the creative industry which covers but is not limited to creative writing (screen and stage), poetry, performing and visual art appreciation. Our intent is to start from the neglected and unrecognized but raw talent of universities undergraduates, amateur secondary school talents and skilled/semi-skilled graduates. We are given to support the creatively inspired who can’t find their foot in the creative industry because they lack support.</p>
						  <p>We have been awakened to the knowledge that while we are fearful to venture into a productive career for lack of funds, what we lack first is the right and apt information, motivation, support and drive. <br>We envision being a hive, that locus where these can be harnessed to spur our collective creative artistry. Hence our motto resonate our mission “Energizing Creative Artistry”</p>
						</details><br><br>
						<div class="clearfix"></div>

						<div class="heading-block fancy-title nobottomborder title-bottom-border">
							<h4>How will <span>We Get there</span>.</h4>
						</div>

						<p>By creating a platform that energizes the creative spirit of every creative seeker. Promoting their creativity, one person at a time, we will lend a voice to the struggle and a hand to the hustle. This we will do by:
						<details>
							<summary><a class="button button-3d noleftmargin">Read More</a></summary><br>
						<p>
						<ul>
							<li>Support: We will make our platform accessible to all who seek to learn the ropes of the industry through partnership with organizations within and outside the country in providing students and graduates with artistic knowledge acquisition and financial resources to enhance their skills.</li>
							<li>Train: We will engage in training students to industry standard and beyond. On the long run, this will be free for undergraduates and subsidized for graduates but for a start we will concentrate on undergraduates and senior secondary school level.</li>
							<li>Educate: Through our creative arm, we will create a vibrant online platform that allows all to learn in a harmonious environment. We will also see to hosting healthy competitions online and in the real sense of the society. We hope to help the youths find an appetite for good vibes and change in stereotypes. A part of our role in this would be to hold a diversified conference once each year, hold a workshop that train participants on other crafts within the art sphere.</li>
							<li>Promote: We will not only be there to guide any creative youth who seeks to be better, we will also promote them till their testimony inspire others. We will promote undergraduate theatre productions (departmental or students projects), writers (literary and screenwriters) through conscious awareness campaign and review of their crafts. This includes other independent genius in art and crafts.</li>
							<li>We will implement these in stages as our capacity increases and resources abound. These are our blueprint for now and the future</li>
						</ul></p>
						</details><br>
						<div class="clearfix"></div>
					</div>

				</div>

				
				<!-- <div class="section nomargin">
					<div class="container clearfix">

					<div class="heading-block center">
						<h4>Our Sponsors</h4>
					</div>

					<ul class="clients-grid grid-6 nobottommargin clearfix">
						<li><a href="http://logofury.com/logo/enzo.php"><img src="canvashtml-cdn.semicolonweb.com/images/clients/1.png" alt="Clients"></a></li>
						<li><a href="http://logofury.com/"><img src="canvashtml-cdn.semicolonweb.com/images/clients/2.png" alt="Clients"></a></li>
						<li><a href="http://logofaves.com/2014/03/grabbt/"><img src="canvashtml-cdn.semicolonweb.com/images/clients/3.png" alt="Clients"></a></li>
						<li><a href="http://logofaves.com/2014/01/ladera-granola/"><img src="canvashtml-cdn.semicolonweb.com/images/clients/4.png" alt="Clients"></a></li>
						<li><a href="http://logofaves.com/2014/02/hershel-farms/"><img src="canvashtml-cdn.semicolonweb.com/images/clients/5.png" alt="Clients"></a></li>
						<li><a href="http://logofury.com/logo/enzo.php"><img src="canvashtml-cdn.semicolonweb.com/images/clients/1.png" alt="Clients"></a></li>
					</ul>

					</div>
				</div> -->

				<div class="section nomargin" style="padding-top: 80px;">
					<div class="heading-block center">
						<h2>The Team Players</h2>
					</div>
				

					<div id="oc-team-list" class="container owl-carousel team-carousel carousel-widget" data-margin="30" data-nav="false" data-items-sm="1" data-items-lg="2" style="padding-bottom: 40px;">

						<div class="oc-item">
							<div class="team team-list clearfix">
								<div class="team-image">
									<img src="images/esther.jpg" alt="Esther Okoloeze">
								</div>
								<div class="team-desc">
									<div class="team-title"><h4>Esther Okoloeze</h4><span>Founder, Leader, Writer and Multitasking Learner</span></div>
									<div class="team-content">
										<p>Esther with the stuck nickname HRM Queen Esther is a Second Class Upper graduate of Theatre Arts from the University of Benin, Benin City. She is currently a distant learning Masters student in Media and Mass Communication Management. she speaks English, Igbo, Yoruba and Nigerian Pidgin fluently and Her interest spans through Film, Art, Literature, Creative writing for stage and screen, Communication, Language and Entrepreneurship.</p>
									</div>
									<br>
										<ul style="text-decoration:none; margin: 0 auto; ">
											<li style="display:inline-block;">
												<a href="https://www.facebook.com/Queen.Esther.Okoloeze" target="_blank"><img src="images/fb.png"></a>
											</li>
											<li style="display:inline-block;">
												<a href="https://twitter.com/esther_okoloeze" target="_blank"><img src="images/tw.png"></a>
											</li>
											<li style="display:inline-block;">
												<a href="https://www.instagram.com/kween_estta/" target="_blank"><img src="images/insta.png"></a>
											</li>
											<li style="display:inline-block;">
												<a href="https://www.linkedin.com/in/esther-okoloeze-15a3b8105" target="_blank"><img src="images/link.png"></a>
											</li>


										</ul>
								</div>
							</div>
						</div>
						<div class="oc-item">
							<div class="team team-list">
								<div class="team-image">
									<img src="images/ebere.jpg" alt="Ebere Akuche Vivian">
								</div>
								<div class="team-desc">
									<div class="team-title"><h4>Ebere Akuche Vivian</h4><span>Creative Editor and Literature Review Analyst</span></div>
									<div class="team-content">
										<p>Vivian Akuche also known as ExplicitEva is an Anambra born, Lagos bred. She is an undergraduate of Mass communication from Federal University of Technology, Akure (FUTO).Her interests are in History, Literature, Music and Art. She boasts of finesse skill in writing and graphics design. She enjoys Reading, Travelling and Listening to music.She aspires to be a Counselor, a journalist and an Activist in the not so far future.</p>
									</div>
									<br>
										<ul style="text-decoration:none; margin: 0 auto; ">
											<li style="display:inline-block;">
												<a href="https://www.facebook.com/vivianakuche" target="_blank"><img src="images/fb.png"></a>
											</li>
											<li style="display:inline-block;">
												<a href="https://twitter.com/evakuche" target="_blank"><img src="images/tw.png"></a>
											</li>
											<li style="display:inline-block;">
												<a href="https://www.instagram.com/evakuche/" target="_blank"><img src="images/insta.png"></a>
											</li>
											<li style="display:inline-block;">
												<a href="https://www.linkedin.com/in/ebere-akuche-b8a48114a" target="_blank"><img src="images/link.png"></a>
											</li>
										</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="promo promo-dark promo-flat promo-full footer-stick"  style="background-color:#0087BD;">
					<div class="container clearfix text-center">
						<h3 style="font-size: 18px;">Call us today at <span>08012345678</span> or Email us at <span>info@seekerslocus.com</span></h3>
					</div>
				</div>			
				<!--<div class="section footer-stick">

					<h4 class="uppercase center">What <span>Clients</span> Say?</h4>

					<div class="fslider testimonial testimonial-full" data-animation="fade" data-arrows="false">
						<div class="flexslider">
							<div class="slider-wrap">
								<div class="slide">
									<div class="testi-image">
										<a href="#"><img src="canvashtml-cdn.semicolonweb.com/images/testimonials/3.jpg" alt="Customer Testimonails"></a>
									</div>
									<div class="testi-content">
										<p>SL Kreativez, kudos to you for this initiative, We would surely continue to support your efforts in everyway we can</p>
										<div class="testi-meta">
											John Doe
											<span>Lagos, Nigeria</span>
										</div>
									</div>
								</div>
								<div class="slide">
									<div class="testi-image">
										<a href="#"><img src="canvashtml-cdn.semicolonweb.com/images/testimonials/2.jpg" alt="Customer Testimonails"></a>
									</div>
									<div class="testi-content">
										<p>SL Kreativez, kudos to you for this initiative, We would surely continue to support your efforts in everyway we can</p>
										<div class="testi-meta">
											John Doe
											<span>Lagos, Nigeria</span>
										</div>
									</div>
								</div>
								<div class="slide">
									<div class="testi-image">
										<a href="#"><img src="canvashtml-cdn.semicolonweb.com/images/testimonials/1.jpg" alt="Customer Testimonails"></a>
									</div>
									<div class="testi-content">
										<p>SL Kreativez, kudos to you for this initiative, We would surely continue to support your efforts in everyway we can</p>
										<div class="testi-meta">
											John Doe
											<span>Lagos, Nigeria</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div> -->

			</div>

		</section><!-- #content end -->

		
		<!-- Footer
		============================================= -->
<?php include('footer.php'); ?>