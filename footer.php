		<section style="padding:80px 0;">
			<div class="heading-block bottommargin-sm" align="center">
				<h3>We are Social!</h3>
			</div>
			<div class="container">
				<div class="col-md-3" style="margin-bottom: 20px;">
					<a href="https://www.facebook.com/seekerslocus" target="_blank"><img src="images/fb2.png"><span style="font-size: 20px; "> SeekersLocus</span></a>
				</div>
				<div class="col-md-3" style="margin-bottom: 20px;">
					<a href="https://www.twitter.com/seekerslocus" target="_blank"><img src="images/tw2.png"><span style="font-size: 20px; "> SeekersLocus</span></a>
				</div>
				<div class="col-md-3" style="margin-bottom: 20px;">
					<a href="https://www.instagram.com/vivianakuche" target="_blank"><img src="images/insta2.png"><span style="font-size: 20px; "> SeekersLocus</span></a>
				</div>
				<div class="col-md-3" style="margin-bottom: 20px;">
					<a href="https://www.linkedin.com/vivianakuche" target="_blank"><img src="images/link2.png"><span style="font-size: 20px; "> SeekersLocus</span></a>
				</div>
			
			</div>
		</section>

<!-- Footer
		============================================= -->
		<footer id="footer" class="dark">

			<div class="container">

				<!-- Footer Widgets
				============================================= -->
				<div class="footer-widgets-wrap clearfix" >

					
					<div class="col-md-6 widget subscribe-widget clearfix" style="margin-bottom: 50px;">
						<h4><img src="images/copy2.jpg"> Copyright</h4> 
						<p align="justify">Hi, as you know most of the information and contents on this website are the intellectual properties of creative individuals. It wouldn't be illegal to use their work without  proper request and attribution. 
						To this end, all contents on seekerslocus.com and other information contained are the exclusive property of SL KREATIVEZ and her partners. They can be shared on social media but any other use must come with express permission from us. When used for educational purposes, users must reference us appropriately. Any commercial use must be with a permission of copyright use. We take the right permission when we use contents from third parties, and they’re properly attributed. <br>You can reach us via info@seekerslocus.com, Thank You.
						</p>
						<div class="clearfix"></div>
						<br>
						<h4><strong>Subscribe</strong> to Our Newsletter to get Important News and latest opporrtunities:</h4>
						<div class="widget-subscribe-form-result"></div>
						<form id="widget-subscribe-form" action="http://themes.semicolonweb.com/html/canvas/include/subscribe.php" role="form" method="post" class="nobottommargin">
							<div class="input-group divcenter">
								
								<input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Enter your Email" style="height:40px;">
								<span class="input-group-btn">
									<button class="button button-3d noleftmargin" style="margin-top:0px;" type="submit">Subscribe</button>
								</span>
							</div>
						</form>
					</div>


					<div class="col-md-6 widget subscribe-widget clearfix" style="margin-top:-5px">
						<h4><img src="images/insta2.jpg"> INSTAGRAM FEEDS </h4>
						<p>
							<iframe src="//users.instush.com/bee-gallery-v2/?cols=5&rows=2&size=small&border=10&round=false&space=4&sl=true&bg=transparent&brd=true&na=false&pin=true&hc=e72476&ltc=3f3f3f&lpc=ffffff&fc=ffffff&user_id=1474819067&username=_ujunwa&sid=-1&susername=-1&tag=-1&stype=mine&t=999999O3mfm-kwleBjgBevNAu0_y4fA-y4hJ8dX5Bx-BUc1Tun1lYnHYIOerZYpdh0sJr5hP59mHe15gs" allowtransparency="true" frameborder="0" scrolling="no"  style="display:block;width:548px;height:224px;border:none;overflow:visible;" ></iframe>
						</p>

						
						<h4>Graphics Designer:<span style="font-size: 13px;"> Samson Ochuko, professional graphics artist and CEO of Phebony Graphics for the Seamless logo and site banner.</span></h4>
						<h4>Images:<span style="font-size: 13px;">pixabay.com, pexels.com for the free to use stock images.</span></h4>
					</div>

				</div><!-- .footer-widgets-wrap end -->

			</div>

			<!-- Copyrights
			============================================= -->
			<div style="margin-bottom:0px; padding-top: 40px; background-color: rgba(0,0,0,0.2);">

				<div class="container clearfix">

					<div class="col_full widget subscribe-widget clearfix" align="center">
						<h4>Copyrights SL Kreativez &copy; 2017 | <span style="font-size: 13px;"> Designed by Rosie Andre (iro3 designs) </h4>
					</div>
	

				</div>

			</div><!-- #copyrights end -->

		</footer><!-- #footer end -->

	</div><!-- #wrapper end -->

	<!-- Go To Top
	============================================= -->
	<div id="gotoTop" class="icon-angle-up"></div>

	<!-- External JavaScripts
	============================================= -->
	<script type="text/javascript" src="assets/js/jquery.js"></script>

	<!-- Footer Scripts
	============================================= -->
	<script type="text/javascript" src="assets/js/functions.js"></script>

	<script type="text/javascript" src="assets/js/new.js"></script>

	
<script>jQuery(document).ready(function(e){e("#primary-menu > ul li").find('.new-badge').children("div").append('<span class="label label-danger" style="display:inline-block;margin-left:8px;position:relative;top:-1px;text-transform:none;">New</span>')});</script>

</html>